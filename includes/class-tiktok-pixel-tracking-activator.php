<?php
if ( !defined( 'ABSPATH' ) ) exit;

class TikTok_Pixel_Tracking_Activator {

    // Fired during plugin activation.
    public static function activate() 
    {

        // Register marketer role
        $marketer = add_role(
            'marketer',
            __( 'Marketer', TIKTOK_PIXEL_TRACKING_TEXTDOMAIN ),
            array(
                'read' => true,
                'manage_tiktok_pixel_tracking' => true,
            )
        );

    }

}
