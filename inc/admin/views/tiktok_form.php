<?php

global $wpdb;

$tiktok_pixel_id = $wpdb->get_var("SELECT meta_value FROM wp_usermeta WHERE meta_key = 'tiktok'");

$tt_pixel_id = isset($tiktok_pixel_id) ? $tiktok_pixel_id : '';

?>

<div class="wrap">
    <h1><?php echo esc_html( get_admin_page_title() ); ?></h1><br>

    <?php if ( isset( $_GET['nds_admin_add_notice'] ) == 'success' ) : ?>
        <div class="notice notice-success is-dismissible">
            <p><?php esc_html_e( 'Settings saved successfully' ); ?></p>
        </div>
    <?php endif; ?>

    <?php
    $current_user_id_tt = get_current_user_id();

    $tiktok_add_meta_nonce = wp_create_nonce('tiktok_add_user_meta_form_nonce');
    ?>

    <form action="<?php echo esc_url(admin_url('admin-post.php')); ?>" method="post" id="tiktok_add_user_meta_form">
        <input type="hidden" name="action" value="tiktok_form_response">
        <input type="hidden" name="tiktok_add_user_meta_nonce" value="<?php echo $tiktok_add_meta_nonce ?>" />
        <input type="hidden" name="<?php echo "tiktok"; ?>[user_meta_key]" value="TikTok" id="<?php echo "TikTok Pixel Tracking"; ?>-user_meta_key" />
        <input type="hidden" name="<?php echo "tiktok"; ?>[user_select]" value="<?php echo $current_user_id_tt ?>" id="<?php echo "TikTok Pixel Tracking"; ?>-user_select" />

        <table class="wp-list-table widefat fixed striped">
            <thead>
                <tr>
                    <th><strong>TikTok Pixel ID</strong></th>
                    <th>
						<input
								type="text"
								id="<?php echo "TikTok Pixel Tracking"; ?>-user_meta_value"
								name="<?php echo "tiktok"; ?>[user_meta_value]"
								style="width: 100%; padding-left: 10px"
								value="<?=$tt_pixel_id?>"
								placeholder="<?php _e('TikTok Pixel ID Value', "TikTok Pixel Tracking"); ?>"
							>
                    </th>
                </tr>
            </thead>
        </table>

        <p class="submit"><input type="submit" name="submit" id="submit" class="button button-primary" value="Submit Pixel"></p>
    </form>
</div>
