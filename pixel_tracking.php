<?php
/*
 * Plugin Name:       TikTok Pixel Tracking
 * Plugin URI:        https://ilias.my/
 * Description:       A simple plugin to track TikTok Pixel Event
 * Version:           1.1.0
 * Author:            Ilias Daniel
 * Author URI:        https://ilias.my/
 * License:           MIT
*/

require 'plugin-update-checker/plugin-update-checker.php';
$myUpdateChecker = Puc_v4_Factory::buildUpdateChecker(
	'https://gitlab.com/iliasgrobox/pixel_tracking/',
	__FILE__,
	'fb_tt_pixel_tracking'
);

$myUpdateChecker->setBranch('main');   


define( 'TIKTOK_PIXEL_TRACKING_FILE', __FILE__ );
define( 'TIKTOK_PIXEL_TRACKING_PATH', plugin_dir_path( TIKTOK_PIXEL_TRACKING_FILE ) );
define( 'TIKTOK_PIXEL_TRACKING_TEXTDOMAIN', 'tiktok-pixel-tracking' );

// Run during plugin activation
function activate_tiktok_pixel_tracking() 
{
    require_once( TIKTOK_PIXEL_TRACKING_PATH . 'includes/class-tiktok-pixel-tracking-activator.php' );
    TikTok_Pixel_Tracking_Activator::activate();
}
register_activation_hook( __FILE__, 'activate_tiktok_pixel_tracking' );


/**
 * Allow customers to access wp-admin
 */
add_filter( 'woocommerce_prevent_admin_access', '__return_false' );
add_filter( 'woocommerce_disable_admin_bar', '__return_false' );

#add new permission on role admin
$role = get_role( 'administrator' );

$role->add_cap( 'manage_tiktok_pixel_tracking' ); 

#overiride permission if already have role marketer
$role = get_role( 'marketer' );

$role->add_cap( 'manage_tiktok_pixel_tracking' ); 

// #admin view
add_action("admin_menu", "addMenu");

function addMenu()
{
  add_menu_page("TikTok Pixel Tracking", "TikTok Pixel Tracking", "manage_tiktok_pixel_tracking", "tiktok-pixel", "form_tiktok", "dashicons-chart-line" );
}

function form_tiktok()
{
    include_once( 'inc/admin/views/tiktok_form.php' );
}

add_action( 'admin_post_tiktok_form_response', 'the_form_response');


function get_full_website_url()
{
    if(isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] === 'on')   
    {
        $url = "https://";   
    }
    else 
    {
        $url = "http://";   
    } 
    // Append the host(domain name, ip) to the URL.   
    $url.= $_SERVER['HTTP_HOST'];   

    // Append the requested resource location to the URL   
    $url.= $_SERVER['REQUEST_URI'];    
    
    return $url;  
}

add_action( 'wp_head','print_tiktok_pixel_on_head', 1 );

function print_tiktok_pixel_on_head()
{

    global $wpdb;

    $tiktok = $wpdb->get_var("SELECT meta_value FROM wp_usermeta WHERE meta_key = 'tiktok'");
    
    $tiktok_pixel_id = isset($tiktok)  ? $tiktok : NULL;
	
    if(isset($tiktok_pixel_id) && $tiktok_pixel_id != NULL)
    {
        ?>
        <!-- TikTok Pixel Code Start -->
        <script>
            !function (w, d, t) 
            {
                w.TiktokAnalyticsObject=t;var ttq=w[t]=w[t]||[];ttq.methods=["page","track","identify","instances","debug","on","off","once","ready","alias","group","enableCookie","disableCookie"],ttq.setAndDefer=function(t,e){t[e]=function(){t.push([e].concat(Array.prototype.slice.call(arguments,0)))}};for(var i=0;i<ttq.methods.length;i++)ttq.setAndDefer(ttq,ttq.methods[i]);ttq.instance=function(t){for(var e=ttq._i[t]||[],n=0;n<ttq.methods.length;n++)ttq.setAndDefer(e,ttq.methods[n]);return e},ttq.load=function(e,n){var i="https://analytics.tiktok.com/i18n/pixel/events.js";ttq._i=ttq._i||{},ttq._i[e]=[],ttq._i[e]._u=i,ttq._t=ttq._t||{},ttq._t[e]=+new Date,ttq._o=ttq._o||{},ttq._o[e]=n||{};var o=document.createElement("script");o.type="text/javascript",o.async=!0,o.src=i+"?sdkid="+e+"&lib="+t;var a=document.getElementsByTagName("script")[0];a.parentNode.insertBefore(o,a)};
            
                ttq.load(<?php print "'$tiktok_pixel_id'"; ?>);
                ttq.page();
            }
            (window, document, 'ttq');
        </script>
        <script>
            ttq.track('Browse')
        </script>
        <!-- TikTok Pixel Code End -->

        <?php
    }

	
}

add_action('wp_body_open', 'print_tiktok_pixel_on_body');

function print_tiktok_pixel_on_body()
{
    global $wpdb;

    $web_full_url = get_full_website_url();

    $tiktok = $wpdb->get_var("SELECT meta_value FROM wp_usermeta WHERE meta_key = 'tiktok'");

    $tiktok_pixel_id = isset($tiktok)  ? $tiktok : NULL;

    if(isset($tiktok_pixel_id) && $tiktok_pixel_id != NULL && strpos($web_full_url, 'thank') !== false)
    {
        ?>
        <script>

            ttq.track('CompletePayment');

        </script>
        <?php
    }
    else if(isset($tiktok_pixel_id) && $tiktok_pixel_id != NULL)
    {
        ?>        
        <!-- Scrolling Pixel -->
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.1/jquery.min.js"></script>
        
        <script>

            console.log('<?=$web_full_url?>');

            ttq.track('ViewContent');

            var isAct = 'start';

            function tiktok_pixel_event(event_name=null)
            {
                ttq.track(event_name);
            }

            function find_suku() 
            {

                var available = $(document).height();
                var percentage_of_page = 0.15;
                var suku_screen = available * percentage_of_page;

                return suku_screen;

            }

            function find_half() 
            {

                var available = $(document).height();
                var percentage_of_page = 0.5;
                var half_screen = available * percentage_of_page;

                return half_screen;

            }

            function find_bottom() 
            {

                var available = $(document).height();
                var percentage_of_page = 0.8;
                var bottom_screen = available * percentage_of_page;

                return bottom_screen;

            }

            $(window).ready(function (e) 
            {
                setTimeout(function()
                {
                    var event_name = 'Subscribe';
                    tiktok_pixel_event(event_name);
                }, 
                20000);
            } 
            );

            $(window).scroll(function (e) 
            {
                var available = $(document).height();

                var height = $(window).scrollTop();

                var suku_screen = find_suku();

                var half_screen = find_half();

                var bottom_screen = find_bottom();

                if (height >= suku_screen  && isAct == 'start')
                {
                    var event_name = 'CompleteRegistration';
                    isAct = 'suku';
                    tiktok_pixel_event(event_name);
                }
                
                if (height >= half_screen  && isAct == 'suku') 
                {
                    var event_name = 'Search';
                    isAct = 'separuh';
                    tiktok_pixel_event(event_name);
                }

                if (height >= bottom_screen  && isAct == 'separuh') 
                {
                    var event_name = 'ClickButton';
                    isAct = 'end';
                    tiktok_pixel_event(event_name);
                }
            }
            );

        </script>

        <?php
    }
}


function the_form_response() 
{

    global $wpdb;
    
    if( isset( $_POST['tiktok_add_user_meta_nonce'] ) && wp_verify_nonce( $_POST['tiktok_add_user_meta_nonce'], 'tiktok_add_user_meta_form_nonce') ) 
    {
        $nds_user_meta_key = sanitize_key( $_POST['tiktok']['user_meta_key'] );
        $nds_user_meta_value = sanitize_text_field( $_POST['tiktok']['user_meta_value'] );
        $nds_user_id = sanitize_key( $_POST['tiktok']['user_select'] );
        
        // server response
        $data = array('user_id' => $nds_user_id, 'meta_key' => $nds_user_meta_key, 'meta_value' => $nds_user_meta_value);

        #query get titkok_meta_id
        $tiktok_pixel_id = $wpdb->get_var("SELECT umeta_id FROM wp_usermeta WHERE meta_key = 'tiktok'");

        #check exiting tiktok_pixel_id
        if ($tiktok_pixel_id)
        {
            $table_name = 'wp_usermeta';
            $meta_value = $data['meta_value'];
            $wpdb->query($wpdb->prepare("UPDATE $table_name SET meta_value = '$meta_value' WHERE umeta_id = '$tiktok_pixel_id'"));
        }
        else
        {
            $wpdb->insert('wp_usermeta', $data);
        }
        // print_r($data);die;
        $admin_notice = "success";
        custom_redirect( $admin_notice, $_POST );
        exit;
    }			
    else 
    {
        wp_die( __( 'Invalid nonce specified', 'tiktok-pixel' ), __( 'Error', 'tiktok-pixel' ), array(
                    'response' 	=> 403,
                    'back_link' => 'admin.php?page=' . 'tiktok-pixel',

            ) );
    }
}


function custom_redirect( $admin_notice, $response ) 
{
		wp_redirect( esc_url_raw( add_query_arg( array(
									'nds_admin_add_notice' => $admin_notice,
									'nds_response' => $response,
									),
							admin_url('admin.php?page=tiktok-pixel' ) 
					) ) );

}